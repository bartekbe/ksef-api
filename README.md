# OpenAPIClient-php

Krajowy System e-Faktur

For more information, please visit [https://ksef.mf.gov.pl](https://ksef.mf.gov.pl).

## Installation & Usage

### Requirements

PHP 7.4 and later.
Should also work with PHP 8.0.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneFakturyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$kse_f_reference_number = 'kse_f_reference_number_example'; // string

try {
    $result = $apiInstance->onlineInvoiceInvoiceGet($kse_f_reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneFakturyApi->onlineInvoiceInvoiceGet: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://ksef.mf.gov.pl/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*InterfejsyInteraktywneFakturyApi* | [**onlineInvoiceInvoiceGet**](docs/Api/InterfejsyInteraktywneFakturyApi.md#onlineinvoiceinvoiceget) | **GET** /online/Invoice/Get/{KSeFReferenceNumber} | Pobranie faktury
*InterfejsyInteraktywneFakturyApi* | [**onlineInvoiceInvoiceSend**](docs/Api/InterfejsyInteraktywneFakturyApi.md#onlineinvoiceinvoicesend) | **PUT** /online/Invoice/Send | Wysyłka faktury
*InterfejsyInteraktywneFakturyApi* | [**onlineInvoiceInvoiceStatus**](docs/Api/InterfejsyInteraktywneFakturyApi.md#onlineinvoiceinvoicestatus) | **GET** /online/Invoice/Status/{InvoiceElementReferenceNumber} | Sprawdzenie statusu wysłanej faktury
*InterfejsyInteraktywnePatnociApi* | [**onlinePaymentPaymentIdentifierGetReferenceNumbers**](docs/Api/InterfejsyInteraktywnePatnociApi.md#onlinepaymentpaymentidentifiergetreferencenumbers) | **GET** /online/Payment/Identifier/GetReferenceNumbers/{PaymentIdentifier} | Pobranie listy faktur dla identyfikatora płatności
*InterfejsyInteraktywnePatnociApi* | [**onlinePaymentPaymentIdentifierRequest**](docs/Api/InterfejsyInteraktywnePatnociApi.md#onlinepaymentpaymentidentifierrequest) | **POST** /online/Payment/Identifier/Request | Wygenerowanie identyfikatora płatności
*InterfejsyInteraktywnePowiadczeniaApi* | [**onlineCredentialsCredentialsContextGrant**](docs/Api/InterfejsyInteraktywnePowiadczeniaApi.md#onlinecredentialscredentialscontextgrant) | **POST** /online/Credentials/ContextGrant | Nadanie poświadczeń kontekstowych
*InterfejsyInteraktywnePowiadczeniaApi* | [**onlineCredentialsCredentialsContextRevoke**](docs/Api/InterfejsyInteraktywnePowiadczeniaApi.md#onlinecredentialscredentialscontextrevoke) | **POST** /online/Credentials/ContextRevoke | Odebranie poświadczeń kontekstowych
*InterfejsyInteraktywnePowiadczeniaApi* | [**onlineCredentialsCredentialsGrant**](docs/Api/InterfejsyInteraktywnePowiadczeniaApi.md#onlinecredentialscredentialsgrant) | **POST** /online/Credentials/Grant | Nadanie poświadczeń
*InterfejsyInteraktywnePowiadczeniaApi* | [**onlineCredentialsCredentialsRevoke**](docs/Api/InterfejsyInteraktywnePowiadczeniaApi.md#onlinecredentialscredentialsrevoke) | **POST** /online/Credentials/Revoke | Odebranie poświadczeń
*InterfejsyInteraktywnePowiadczeniaApi* | [**onlineCredentialsCredentialsStatus**](docs/Api/InterfejsyInteraktywnePowiadczeniaApi.md#onlinecredentialscredentialsstatus) | **GET** /online/Credentials/Status/{CredentialsElementReferenceNumber} | Sprawdzenie statusu poświadczeń
*InterfejsyInteraktywnePowiadczeniaApi* | [**onlineCredentialsGenerateToken**](docs/Api/InterfejsyInteraktywnePowiadczeniaApi.md#onlinecredentialsgeneratetoken) | **POST** /online/Credentials/GenerateToken | Generowanie tokena autoryzacyjnego
*InterfejsyInteraktywnePowiadczeniaApi* | [**onlineCredentialsRevokeToken**](docs/Api/InterfejsyInteraktywnePowiadczeniaApi.md#onlinecredentialsrevoketoken) | **POST** /online/Credentials/RevokeToken | Usunięcie tokena autoryzacyjnego
*InterfejsyInteraktywneSesjaApi* | [**onlineSessionAuthorisationChallenge**](docs/Api/InterfejsyInteraktywneSesjaApi.md#onlinesessionauthorisationchallenge) | **POST** /online/Session/AuthorisationChallenge | Inicjalizacja mechanizmu uwierzytelnienia i autoryzacji
*InterfejsyInteraktywneSesjaApi* | [**onlineSessionSessionSignedInit**](docs/Api/InterfejsyInteraktywneSesjaApi.md#onlinesessionsessionsignedinit) | **POST** /online/Session/InitSigned | Inicjalizacja sesji, wskazanie kontekstu, uwierzytelnienie i autoryzacja
*InterfejsyInteraktywneSesjaApi* | [**onlineSessionSessionStatusPlain**](docs/Api/InterfejsyInteraktywneSesjaApi.md#onlinesessionsessionstatusplain) | **GET** /online/Session/Status | Sprawdzenie statusu aktywnej sesji interaktywnej
*InterfejsyInteraktywneSesjaApi* | [**onlineSessionSessionStatusReferenceNumber**](docs/Api/InterfejsyInteraktywneSesjaApi.md#onlinesessionsessionstatusreferencenumber) | **GET** /online/Session/Status/{ReferenceNumber} | Sprawdzenie statusu sesji ogólnej
*InterfejsyInteraktywneSesjaApi* | [**onlineSessionSessionTerminatePlain**](docs/Api/InterfejsyInteraktywneSesjaApi.md#onlinesessionsessionterminateplain) | **GET** /online/Session/Terminate | Wymuszenie zamknięcia aktywnej sesji
*InterfejsyInteraktywneSesjaApi* | [**onlineSessionSessionTokenInit**](docs/Api/InterfejsyInteraktywneSesjaApi.md#onlinesessionsessiontokeninit) | **POST** /online/Session/InitToken | Inicjalizacja sesji, wskazanie kontekstu, uwierzytelnienie i autoryzacja
*InterfejsyInteraktywneZapytaniaApi* | [**onlineQueryQueryCredentials**](docs/Api/InterfejsyInteraktywneZapytaniaApi.md#onlinequeryquerycredentials) | **POST** /online/Query/Credential/Sync | Zapytanie o poświadczenia
*InterfejsyInteraktywneZapytaniaApi* | [**onlineQueryQueryInvoice**](docs/Api/InterfejsyInteraktywneZapytaniaApi.md#onlinequeryqueryinvoice) | **POST** /online/Query/Invoice/Sync | Zapytanie o faktury
*InterfejsyInteraktywneZapytaniaApi* | [**onlineQueryQueryInvoiceFetch**](docs/Api/InterfejsyInteraktywneZapytaniaApi.md#onlinequeryqueryinvoicefetch) | **GET** /online/Query/Invoice/Async/Fetch/{QueryElementReferenceNumber}/{PartElementReferenceNumber} | Pobranie wyników zapytania o faktury
*InterfejsyInteraktywneZapytaniaApi* | [**onlineQueryQueryInvoiceInit**](docs/Api/InterfejsyInteraktywneZapytaniaApi.md#onlinequeryqueryinvoiceinit) | **POST** /online/Query/Invoice/Async/Init | Inicjalizacja zapytania o faktury
*InterfejsyInteraktywneZapytaniaApi* | [**onlineQueryQueryInvoiceStatus**](docs/Api/InterfejsyInteraktywneZapytaniaApi.md#onlinequeryqueryinvoicestatus) | **GET** /online/Query/Invoice/Async/Status/{QueryElementReferenceNumber} | Sprawdzenie statusu zapytania o faktury

## Models

- [AuthorisationChallengeRequest](docs/Model/AuthorisationChallengeRequest.md)
- [AuthorisationChallengeResponse](docs/Model/AuthorisationChallengeResponse.md)
- [CredentialsBaseTypeObject](docs/Model/CredentialsBaseTypeObject.md)
- [CredentialsIdentifierRequestIndividualCertificateFingerprintType](docs/Model/CredentialsIdentifierRequestIndividualCertificateFingerprintType.md)
- [CredentialsIdentifierRequestIndividualNipType](docs/Model/CredentialsIdentifierRequestIndividualNipType.md)
- [CredentialsIdentifierRequestIndividualPeselType](docs/Model/CredentialsIdentifierRequestIndividualPeselType.md)
- [CredentialsIdentifierRequestIndividualType](docs/Model/CredentialsIdentifierRequestIndividualType.md)
- [CredentialsIdentifierRequestInstitutionalNipType](docs/Model/CredentialsIdentifierRequestInstitutionalNipType.md)
- [CredentialsIdentifierRequestInstitutionalType](docs/Model/CredentialsIdentifierRequestInstitutionalType.md)
- [CredentialsIdentifierRequestType](docs/Model/CredentialsIdentifierRequestType.md)
- [CredentialsIdentifierResponseAuthorisationTokenType](docs/Model/CredentialsIdentifierResponseAuthorisationTokenType.md)
- [CredentialsIdentifierResponseIndividualCertificateFingerprintType](docs/Model/CredentialsIdentifierResponseIndividualCertificateFingerprintType.md)
- [CredentialsIdentifierResponseIndividualNipType](docs/Model/CredentialsIdentifierResponseIndividualNipType.md)
- [CredentialsIdentifierResponseIndividualPeselType](docs/Model/CredentialsIdentifierResponseIndividualPeselType.md)
- [CredentialsIdentifierResponseInstitutionalNipType](docs/Model/CredentialsIdentifierResponseInstitutionalNipType.md)
- [CredentialsIdentifierResponseSystemType](docs/Model/CredentialsIdentifierResponseSystemType.md)
- [CredentialsIdentifierResponseType](docs/Model/CredentialsIdentifierResponseType.md)
- [CredentialsParentType](docs/Model/CredentialsParentType.md)
- [CredentialsPlainType](docs/Model/CredentialsPlainType.md)
- [CredentialsRoleRequestContextBaseType](docs/Model/CredentialsRoleRequestContextBaseType.md)
- [CredentialsRoleRequestContextDescribedType](docs/Model/CredentialsRoleRequestContextDescribedType.md)
- [CredentialsRoleRequestStandardBaseType](docs/Model/CredentialsRoleRequestStandardBaseType.md)
- [CredentialsRoleRequestStandardDescribedType](docs/Model/CredentialsRoleRequestStandardDescribedType.md)
- [CredentialsRoleRequestTokenType](docs/Model/CredentialsRoleRequestTokenType.md)
- [CredentialsRoleResponseBaseTypeObject](docs/Model/CredentialsRoleResponseBaseTypeObject.md)
- [CredentialsRoleResponseGrantedForInstitutionType](docs/Model/CredentialsRoleResponseGrantedForInstitutionType.md)
- [CredentialsRoleResponseStandardDetailsType](docs/Model/CredentialsRoleResponseStandardDetailsType.md)
- [CredentialsRoleResponseStandardPlainType](docs/Model/CredentialsRoleResponseStandardPlainType.md)
- [CredentialsRoleResponseTokenType](docs/Model/CredentialsRoleResponseTokenType.md)
- [CredentialsTokenType](docs/Model/CredentialsTokenType.md)
- [ExceptionDetailType](docs/Model/ExceptionDetailType.md)
- [ExceptionResponse](docs/Model/ExceptionResponse.md)
- [ExceptionType](docs/Model/ExceptionType.md)
- [File1MBHashType](docs/Model/File1MBHashType.md)
- [File2MBHashType](docs/Model/File2MBHashType.md)
- [FileUnlimitedHashType](docs/Model/FileUnlimitedHashType.md)
- [GenerateTokenRequest](docs/Model/GenerateTokenRequest.md)
- [GenerateTokenRequestType](docs/Model/GenerateTokenRequestType.md)
- [GenerateTokenResponse](docs/Model/GenerateTokenResponse.md)
- [GetPaymentIdentifierReferenceNumbersResponse](docs/Model/GetPaymentIdentifierReferenceNumbersResponse.md)
- [GrantContextCredentialsRequest](docs/Model/GrantContextCredentialsRequest.md)
- [GrantContextCredentialsRequestType](docs/Model/GrantContextCredentialsRequestType.md)
- [GrantCredentialsRequest](docs/Model/GrantCredentialsRequest.md)
- [GrantCredentialsRequestType](docs/Model/GrantCredentialsRequestType.md)
- [HashSHAType](docs/Model/HashSHAType.md)
- [InitSessionResponse](docs/Model/InitSessionResponse.md)
- [InitialisedSessionType](docs/Model/InitialisedSessionType.md)
- [InvoiceDivisionPlainPartType](docs/Model/InvoiceDivisionPlainPartType.md)
- [InvoiceHeaderType](docs/Model/InvoiceHeaderType.md)
- [InvoicePayloadEncryptedType](docs/Model/InvoicePayloadEncryptedType.md)
- [InvoicePayloadPlainType](docs/Model/InvoicePayloadPlainType.md)
- [InvoicePayloadType](docs/Model/InvoicePayloadType.md)
- [InvoiceStatusType](docs/Model/InvoiceStatusType.md)
- [QueryCriteriaCredentialsAllType](docs/Model/QueryCriteriaCredentialsAllType.md)
- [QueryCriteriaCredentialsIdType](docs/Model/QueryCriteriaCredentialsIdType.md)
- [QueryCriteriaCredentialsType](docs/Model/QueryCriteriaCredentialsType.md)
- [QueryCriteriaInvoiceDetailType](docs/Model/QueryCriteriaInvoiceDetailType.md)
- [QueryCriteriaInvoiceIncrementalType](docs/Model/QueryCriteriaInvoiceIncrementalType.md)
- [QueryCriteriaInvoiceRangeType](docs/Model/QueryCriteriaInvoiceRangeType.md)
- [QueryCriteriaInvoiceType](docs/Model/QueryCriteriaInvoiceType.md)
- [QueryInvoiceAsyncInitResponse](docs/Model/QueryInvoiceAsyncInitResponse.md)
- [QueryInvoiceAsyncStatusResponse](docs/Model/QueryInvoiceAsyncStatusResponse.md)
- [QueryInvoiceRequest](docs/Model/QueryInvoiceRequest.md)
- [QueryInvoiceSyncResponse](docs/Model/QueryInvoiceSyncResponse.md)
- [QuerySyncCredentialsRequest](docs/Model/QuerySyncCredentialsRequest.md)
- [QuerySyncCredentialsResponse](docs/Model/QuerySyncCredentialsResponse.md)
- [RequestPaymentIdentifierRequest](docs/Model/RequestPaymentIdentifierRequest.md)
- [RequestPaymentIdentifierResponse](docs/Model/RequestPaymentIdentifierResponse.md)
- [RevokeContextCredentialsRequest](docs/Model/RevokeContextCredentialsRequest.md)
- [RevokeContextCredentialsRequestType](docs/Model/RevokeContextCredentialsRequestType.md)
- [RevokeCredentialsRequest](docs/Model/RevokeCredentialsRequest.md)
- [RevokeCredentialsRequestType](docs/Model/RevokeCredentialsRequestType.md)
- [RevokeTokenRequest](docs/Model/RevokeTokenRequest.md)
- [RevokeTokenRequestType](docs/Model/RevokeTokenRequestType.md)
- [SendInvoiceRequest](docs/Model/SendInvoiceRequest.md)
- [SendInvoiceResponse](docs/Model/SendInvoiceResponse.md)
- [SessionContextType](docs/Model/SessionContextType.md)
- [SessionInvoiceStatusType](docs/Model/SessionInvoiceStatusType.md)
- [SessionStatusResponse](docs/Model/SessionStatusResponse.md)
- [StatusCredentialsResponse](docs/Model/StatusCredentialsResponse.md)
- [StatusInvoiceResponse](docs/Model/StatusInvoiceResponse.md)
- [SubjectAuthorizedType](docs/Model/SubjectAuthorizedType.md)
- [SubjectByType](docs/Model/SubjectByType.md)
- [SubjectFullNameType](docs/Model/SubjectFullNameType.md)
- [SubjectIdentifierByCompanyType](docs/Model/SubjectIdentifierByCompanyType.md)
- [SubjectIdentifierByType](docs/Model/SubjectIdentifierByType.md)
- [SubjectIdentifierToCompanyType](docs/Model/SubjectIdentifierToCompanyType.md)
- [SubjectIdentifierToNoneType](docs/Model/SubjectIdentifierToNoneType.md)
- [SubjectIdentifierToOtherTaxType](docs/Model/SubjectIdentifierToOtherTaxType.md)
- [SubjectIdentifierToType](docs/Model/SubjectIdentifierToType.md)
- [SubjectNameType](docs/Model/SubjectNameType.md)
- [SubjectOtherType](docs/Model/SubjectOtherType.md)
- [SubjectPersonNameType](docs/Model/SubjectPersonNameType.md)
- [SubjectToType](docs/Model/SubjectToType.md)
- [TerminateSessionResponse](docs/Model/TerminateSessionResponse.md)
- [V2CredentialsRoleGrantedForInstitutionInheritanceType](docs/Model/V2CredentialsRoleGrantedForInstitutionInheritanceType.md)
- [V2CredentialsRoleResponseBaseTypeObject](docs/Model/V2CredentialsRoleResponseBaseTypeObject.md)
- [V2CredentialsRoleResponseGrantedByInstitutionType](docs/Model/V2CredentialsRoleResponseGrantedByInstitutionType.md)
- [V2CredentialsRoleResponseGrantedForIndividualDirectType](docs/Model/V2CredentialsRoleResponseGrantedForIndividualDirectType.md)
- [V2CredentialsRoleResponseGrantedForIndividualInheritedType](docs/Model/V2CredentialsRoleResponseGrantedForIndividualInheritedType.md)
- [V2CredentialsRoleResponseGrantedForIndividualTokenType](docs/Model/V2CredentialsRoleResponseGrantedForIndividualTokenType.md)
- [V2InitSessionResponse](docs/Model/V2InitSessionResponse.md)
- [V2InitialisedSessionType](docs/Model/V2InitialisedSessionType.md)
- [V2QueryCriteriaInvoiceDetailType](docs/Model/V2QueryCriteriaInvoiceDetailType.md)
- [V2QueryCriteriaInvoiceIncrementalType](docs/Model/V2QueryCriteriaInvoiceIncrementalType.md)
- [V2QueryCriteriaInvoiceRangeType](docs/Model/V2QueryCriteriaInvoiceRangeType.md)
- [V2QueryCriteriaInvoiceType](docs/Model/V2QueryCriteriaInvoiceType.md)
- [V2QueryInvoiceRequest](docs/Model/V2QueryInvoiceRequest.md)
- [V2RevokeTokenRequest](docs/Model/V2RevokeTokenRequest.md)
- [V2RevokeTokenRequestType](docs/Model/V2RevokeTokenRequestType.md)
- [V2RevokeTokenResponse](docs/Model/V2RevokeTokenResponse.md)
- [V2SessionContextType](docs/Model/V2SessionContextType.md)
- [V2SubjectByQueryType](docs/Model/V2SubjectByQueryType.md)
- [V2SubjectToQueryType](docs/Model/V2SubjectToQueryType.md)

## Authorization

Authentication schemes defined for the API:
### SessionToken

- **Type**: API key
- **API key parameter name**: SessionToken
- **Location**: HTTP header


## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author

info.ksef@mf.gov.pl

## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.1.3`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
