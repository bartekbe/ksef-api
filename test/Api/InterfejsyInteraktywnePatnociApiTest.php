<?php
/**
 * InterfejsyInteraktywnePatnociApiTest
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * KSeF
 *
 * Krajowy System e-Faktur
 *
 * The version of the OpenAPI document: 1.1.3
 * Contact: info.ksef@mf.gov.pl
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.0.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace OpenAPI\Client\Test\Api;

use \OpenAPI\Client\Configuration;
use \OpenAPI\Client\ApiException;
use \OpenAPI\Client\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * InterfejsyInteraktywnePatnociApiTest Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class InterfejsyInteraktywnePatnociApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for onlinePaymentPaymentIdentifierGetReferenceNumbers
     *
     * Pobranie listy faktur dla identyfikatora płatności.
     *
     */
    public function testOnlinePaymentPaymentIdentifierGetReferenceNumbers()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for onlinePaymentPaymentIdentifierRequest
     *
     * Wygenerowanie identyfikatora płatności.
     *
     */
    public function testOnlinePaymentPaymentIdentifierRequest()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
