# OpenAPI\Client\InterfejsyInteraktywneSesjaApi

All URIs are relative to https://ksef.mf.gov.pl/api, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**onlineSessionAuthorisationChallenge()**](InterfejsyInteraktywneSesjaApi.md#onlineSessionAuthorisationChallenge) | **POST** /online/Session/AuthorisationChallenge | Inicjalizacja mechanizmu uwierzytelnienia i autoryzacji |
| [**onlineSessionSessionSignedInit()**](InterfejsyInteraktywneSesjaApi.md#onlineSessionSessionSignedInit) | **POST** /online/Session/InitSigned | Inicjalizacja sesji, wskazanie kontekstu, uwierzytelnienie i autoryzacja |
| [**onlineSessionSessionStatusPlain()**](InterfejsyInteraktywneSesjaApi.md#onlineSessionSessionStatusPlain) | **GET** /online/Session/Status | Sprawdzenie statusu aktywnej sesji interaktywnej |
| [**onlineSessionSessionStatusReferenceNumber()**](InterfejsyInteraktywneSesjaApi.md#onlineSessionSessionStatusReferenceNumber) | **GET** /online/Session/Status/{ReferenceNumber} | Sprawdzenie statusu sesji ogólnej |
| [**onlineSessionSessionTerminatePlain()**](InterfejsyInteraktywneSesjaApi.md#onlineSessionSessionTerminatePlain) | **GET** /online/Session/Terminate | Wymuszenie zamknięcia aktywnej sesji |
| [**onlineSessionSessionTokenInit()**](InterfejsyInteraktywneSesjaApi.md#onlineSessionSessionTokenInit) | **POST** /online/Session/InitToken | Inicjalizacja sesji, wskazanie kontekstu, uwierzytelnienie i autoryzacja |


## `onlineSessionAuthorisationChallenge()`

```php
onlineSessionAuthorisationChallenge($authorisation_challenge_request): \OpenAPI\Client\Model\AuthorisationChallengeResponse
```

Inicjalizacja mechanizmu uwierzytelnienia i autoryzacji

Inicjalizacja mechanizmu uwierzytelnienia i autoryzacji.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneSesjaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$authorisation_challenge_request = {"contextIdentifier":{"type":"onip","identifier":"1111111111"}}; // \OpenAPI\Client\Model\AuthorisationChallengeRequest

try {
    $result = $apiInstance->onlineSessionAuthorisationChallenge($authorisation_challenge_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneSesjaApi->onlineSessionAuthorisationChallenge: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **authorisation_challenge_request** | [**\OpenAPI\Client\Model\AuthorisationChallengeRequest**](../Model/AuthorisationChallengeRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\AuthorisationChallengeResponse**](../Model/AuthorisationChallengeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineSessionSessionSignedInit()`

```php
onlineSessionSessionSignedInit($body): \OpenAPI\Client\Model\InitSessionResponse
```

Inicjalizacja sesji, wskazanie kontekstu, uwierzytelnienie i autoryzacja

Inicjalizacja sesji interaktywnej. Podpisany dokument http://ksef.mf.gov.pl/schema/gtw/svc/online/auth/request/2021/10/01/0001/InitSessionSignedRequest

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneSesjaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->onlineSessionSessionSignedInit($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneSesjaApi->onlineSessionSessionSignedInit: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **body** | **\SplFileObject****\SplFileObject**|  | |

### Return type

[**\OpenAPI\Client\Model\InitSessionResponse**](../Model/InitSessionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/octet-stream`
- **Accept**: `application/json`, `application/vnd.v2+json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineSessionSessionStatusPlain()`

```php
onlineSessionSessionStatusPlain($page_size, $page_offset, $include_details): \OpenAPI\Client\Model\SessionStatusResponse
```

Sprawdzenie statusu aktywnej sesji interaktywnej

Sprawdzenie statusu obecnego przetwarzania interaktywnego

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneSesjaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_size = 10; // int
$page_offset = 0; // int
$include_details = true; // bool

try {
    $result = $apiInstance->onlineSessionSessionStatusPlain($page_size, $page_offset, $include_details);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneSesjaApi->onlineSessionSessionStatusPlain: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page_size** | **int**|  | [optional] [default to 10] |
| **page_offset** | **int**|  | [optional] [default to 0] |
| **include_details** | **bool**|  | [optional] [default to true] |

### Return type

[**\OpenAPI\Client\Model\SessionStatusResponse**](../Model/SessionStatusResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineSessionSessionStatusReferenceNumber()`

```php
onlineSessionSessionStatusReferenceNumber($reference_number, $page_size, $page_offset, $include_details): \OpenAPI\Client\Model\SessionStatusResponse
```

Sprawdzenie statusu sesji ogólnej

Sprawdzenie statusu przetwarzania na podstawie numeru referencyjnego

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneSesjaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$reference_number = 'reference_number_example'; // string
$page_size = 10; // int
$page_offset = 0; // int
$include_details = true; // bool

try {
    $result = $apiInstance->onlineSessionSessionStatusReferenceNumber($reference_number, $page_size, $page_offset, $include_details);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneSesjaApi->onlineSessionSessionStatusReferenceNumber: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **reference_number** | **string**|  | |
| **page_size** | **int**|  | [optional] [default to 10] |
| **page_offset** | **int**|  | [optional] [default to 0] |
| **include_details** | **bool**|  | [optional] [default to true] |

### Return type

[**\OpenAPI\Client\Model\SessionStatusResponse**](../Model/SessionStatusResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineSessionSessionTerminatePlain()`

```php
onlineSessionSessionTerminatePlain(): \OpenAPI\Client\Model\TerminateSessionResponse
```

Wymuszenie zamknięcia aktywnej sesji

Wymuszenie zamknięcia aktywnej sesji interaktywnej

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneSesjaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->onlineSessionSessionTerminatePlain();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneSesjaApi->onlineSessionSessionTerminatePlain: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\TerminateSessionResponse**](../Model/TerminateSessionResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineSessionSessionTokenInit()`

```php
onlineSessionSessionTokenInit($body): \OpenAPI\Client\Model\InitSessionResponse
```

Inicjalizacja sesji, wskazanie kontekstu, uwierzytelnienie i autoryzacja

Inicjalizacja sesji interaktywnej. Zaszyfrowany kluczem publicznym KSeF dokument http://ksef.mf.gov.pl/schema/gtw/svc/online/auth/request/2021/10/01/0001/InitSessionTokenRequest

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneSesjaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->onlineSessionSessionTokenInit($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneSesjaApi->onlineSessionSessionTokenInit: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **body** | **\SplFileObject****\SplFileObject**|  | |

### Return type

[**\OpenAPI\Client\Model\InitSessionResponse**](../Model/InitSessionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/octet-stream`
- **Accept**: `application/json`, `application/vnd.v2+json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
