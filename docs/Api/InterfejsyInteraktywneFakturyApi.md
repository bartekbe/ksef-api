# OpenAPI\Client\InterfejsyInteraktywneFakturyApi

All URIs are relative to https://ksef.mf.gov.pl/api, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**onlineInvoiceInvoiceGet()**](InterfejsyInteraktywneFakturyApi.md#onlineInvoiceInvoiceGet) | **GET** /online/Invoice/Get/{KSeFReferenceNumber} | Pobranie faktury |
| [**onlineInvoiceInvoiceSend()**](InterfejsyInteraktywneFakturyApi.md#onlineInvoiceInvoiceSend) | **PUT** /online/Invoice/Send | Wysyłka faktury |
| [**onlineInvoiceInvoiceStatus()**](InterfejsyInteraktywneFakturyApi.md#onlineInvoiceInvoiceStatus) | **GET** /online/Invoice/Status/{InvoiceElementReferenceNumber} | Sprawdzenie statusu wysłanej faktury |


## `onlineInvoiceInvoiceGet()`

```php
onlineInvoiceInvoiceGet($kse_f_reference_number): object
```

Pobranie faktury

Pobranie faktury

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneFakturyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$kse_f_reference_number = 'kse_f_reference_number_example'; // string

try {
    $result = $apiInstance->onlineInvoiceInvoiceGet($kse_f_reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneFakturyApi->onlineInvoiceInvoiceGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **kse_f_reference_number** | **string**|  | |

### Return type

**object**

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/octet-stream`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineInvoiceInvoiceSend()`

```php
onlineInvoiceInvoiceSend($send_invoice_request): \OpenAPI\Client\Model\SendInvoiceResponse
```

Wysyłka faktury

Wysyłka faktury

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneFakturyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$send_invoice_request = {"invoiceHash":{"hashSHA":{"algorithm":"SHA-256","encoding":"Base64","value":"ndeJt2MSJXAPbidtqM8Hnq7c2iIa7qY/y/dgkJUaQT4="},"fileSize":2135},"invoicePayload":{"type":"encrypted","encryptedInvoiceHash":{"hashSHA":{"algorithm":"SHA-256","encoding":"Base64","value":"CpecTP+QmTAbzC4i1rlJ0VKVZwJSXp/cKxkkvdElFG0="},"fileSize":2144},"encryptedInvoiceBody":"RJ1H3kOesCQHUkcpTi7GTbIQkF0IMdKuUFGrduK+471oGc/HjH3td7M/PXVdoTPDBWIY8oIMrwNbGXEpSBo0muK+iAtt/XOQCsEBiXw1lO0O8YVd62AScttHcx4I9hOlKGSFj4/f8wkuOfM5D7Uoh3vsB0wktx0GvNNAWNM6F9XJ5LRjnIOFGy9hWoWDT2YccFtgxBSmiQD6GS0qm4KnQzVebV6ICPrmaN08d1WdYBod3JEFCA8rFNBN4e617apjvUiLO7IV/vNp5zTps59/NW9DddoiUnYR/AubKwSD93ztcsCM8rcN3XzMnYdJ3rNdg9e8n4UhAMc8DcvuHW8GY9EGcbaVDPaFSlEAJj5ubpbpqOM6Xb1XfeXLlGRn7WLHDhPDyGh+aIKc4a94pXKRzoo6Vc7j8fzrCrIr6TgEr6OmIlfaSS9QBmgn2PyzruC1ORDwgLdWdGLpZz9ic4pGj1rJPxchL5TfJzOunYqCUEt7VMW1V0H4/7cK48HctvHPBKrhu99RxPY7vddGeE2l9o8yPAjFlgYa5z4AkQPH3m3YBW+1WNl57nhlC/R5sv59BnG1W/p/OtFA3PkyNRZNzKh9u74n/1/fWy1jWcMnIuZHnqJdr6MvUBr9w3AL1sSpyv8t7v7nQvlvjFKSHeqi6QFr2umW1DOdwSerttRii571xLLONEpwChzgEMCL0G/o5p8vtD93Gaat7LvY6sxeOR8QJ5AJlif5ViguHsvlTMEnAN9F1O2h3eozkCfrm3Fh9dIrLOOOMOAWLmq8jhHSEp1gUc8RRnYqKFgagSwn8f2kzMGOSlFAY1Wz0ODaZlTKINXtCoDznjdqJ72PP3MPAB7PCw7TJbXsT9RsbfyCX9K0oTEAIe8VQj1rJ//GJ3Uugav2b5eTvMATlAzzLMdkTWoiE/LPZsoBqbq0wJZbVmcInXYZ7eI7x7FGbNO638EWfV146sUOhO6JMAKwBiKU0gMqnE+AgaNBqKve0reVdEUWs2pX2iLaXhLObbuY+ADS8odED1mz0dx4d2bPy9H9uK0WiFLTQU014oP2juYwi4JRln0lbLTYu2+R4R5J/fL1kgkH2Vd8i0wZPZKv+OyrYzT+K1vWB/kbSExw2vuxD4OYXEItiCBN4+rJh57S6hS/7kN1b2X42TiqQhgmh8pDHsjlFwjYns7laxn71VaBDoyBaU3CZj8OJrDwZPP87ZGd5GIukBandjV4/uVSVvuNlsMsfHm7zI8+b2skQpwbtbL8zEYkfqYyh2s/+Aak9wAcPl523QilLaZ89/lRUvXJhjuiHAco5Bxiod9B8jUjvZoaEZu9bNNUuEbuXDPhC8zOqisnTADyVbxQlQo64ZOb0iGbsDzK77TGiAOrYjQM9nXDMCB5QgmQ47jgVB/yt3S+lb6E+k14GwP39LKt+A5Icgl8Ztji28NT7buz8BhQyeTENcafosOkI9xiWh3ZvGqLUG0/pv926bLRLipIBUY4vqRqtAF0fQKyswqI0C6XbCBri1Kb4pv1QEC69h3N7ZlhiiAtJkZ3i1Bjj6H4Qrqw7uvA/ULVkn4Rm35aYwIP/jxf4grNGZt+A28JeXKAT1P4eqRnNYa62uSnVBel1KZJIU9Ql1Zxss7ZtGkBNS/1nlH4/I+BAYBV0zkI86Ib/ZdD4eSHnlEkCbJqtWjW3YfAFGwCcnSqlPWVtmNP+hYuCbGKGXiCmg5oyiJkvxrBH7fvWmPftnrN3pn0SDPm7stO0ogI0ksFfaxzJ+2oc5ckMUYgHYPG56A4tETOQzLlRjWTGlrzsSwkh050K8Ev57vHJ0AxckW9ynlfGBnzV4wAFm91yWhewOzOTaEtTeUJrsP9xoruGQPkglmseujG43Fj4uA76v/X4iZfr4BAHvuAYKOo3GTQK2r6ZYGZSqTskiO0hdBUJxjK4x8OEqMs8Q78WGTZQdEo87/dOwGS4HoBYeETZ5GtAR27LlyLNepQ9TKkaWdhmQ3UF4F5DFX1Sll+d5KLGLJEaGecWuTPEkV5Uaj4mVLogWwFNiaW2XLXi9cIBO+mPIuVOdF+/88xJO1d0UhU3Gksgxu0xm4B1SM0d4uElSnGdL7ClhPbfOHPgFiPudNXqP8A0zEle5l/VR8zIbCFZfJl4UI5xOMjzjQm7yE9ZHgftGEAY2mb74CvxyzHnsfHTGz7YO6oapZFSNBmfQqiWjp27tWpsTT7HkO+/Q44QcFcA2MqGfMjit9cpEdeMdvSwxSMD9AM2lPx1CqF4ZkT2nKvD61Vu+KGg+E/fn9+P00XrQKlzEVrVvHoPq4Uj7K4ll5sG3tFbfKqX9NrjslzO8czVPZlOth4KeGAqxe5XFFZ2fs7rZg9OOjmURFC6dUMUFnZuKxmCsz51LD12l/iPmMLPxUrY8md1FKZNBJ/8SjbxvIk6U6PlEG10+iIPCjcYHmvHz8ORY+AaUfc+5P1XVyJdphX2RyDtyYFNxoJaJ/iZ5O0O/C5ihtv1BH8urtzfBR9tjT9F540CWuQwJPbx8e7VZP9GeMR6wxE20IIHAm3azhtpILjLRSmaCOreMciKt9TRfV7IOmuPOWKdNMV5yO15+kMBOyZTA91f4INpwSsGbUL7mJQL6oW3dMCOdY2OJglDlLs+QZusbiw38lEvNlWruAGQNDqkvOF2srLSkRYklS4+Q1FZKk+E2z8ncvLdsxLCDad2yfq/d+YtBAS6ws5WIaWCsOK4A2uCICQZg0rQ0510pzghsYLYEmBuAiRGbvTbR8UHBankK9m2h3IOijD9wc6Wfov/67zCcTjbz6KJ9OI/3SdI+2+ZU4W+BPni1FtL/wlKJZUcR3Pcq90eLYh2XMridD0djXwQRA="}}; // \OpenAPI\Client\Model\SendInvoiceRequest

try {
    $result = $apiInstance->onlineInvoiceInvoiceSend($send_invoice_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneFakturyApi->onlineInvoiceInvoiceSend: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **send_invoice_request** | [**\OpenAPI\Client\Model\SendInvoiceRequest**](../Model/SendInvoiceRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\SendInvoiceResponse**](../Model/SendInvoiceResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineInvoiceInvoiceStatus()`

```php
onlineInvoiceInvoiceStatus($invoice_element_reference_number): \OpenAPI\Client\Model\StatusInvoiceResponse
```

Sprawdzenie statusu wysłanej faktury

Sprawdzenie statusu wysłanej faktury

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneFakturyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_element_reference_number = 'invoice_element_reference_number_example'; // string

try {
    $result = $apiInstance->onlineInvoiceInvoiceStatus($invoice_element_reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneFakturyApi->onlineInvoiceInvoiceStatus: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **invoice_element_reference_number** | **string**|  | |

### Return type

[**\OpenAPI\Client\Model\StatusInvoiceResponse**](../Model/StatusInvoiceResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
