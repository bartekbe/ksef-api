# OpenAPI\Client\InterfejsyInteraktywnePatnociApi

All URIs are relative to https://ksef.mf.gov.pl/api, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**onlinePaymentPaymentIdentifierGetReferenceNumbers()**](InterfejsyInteraktywnePatnociApi.md#onlinePaymentPaymentIdentifierGetReferenceNumbers) | **GET** /online/Payment/Identifier/GetReferenceNumbers/{PaymentIdentifier} | Pobranie listy faktur dla identyfikatora płatności |
| [**onlinePaymentPaymentIdentifierRequest()**](InterfejsyInteraktywnePatnociApi.md#onlinePaymentPaymentIdentifierRequest) | **POST** /online/Payment/Identifier/Request | Wygenerowanie identyfikatora płatności |


## `onlinePaymentPaymentIdentifierGetReferenceNumbers()`

```php
onlinePaymentPaymentIdentifierGetReferenceNumbers($payment_identifier): \OpenAPI\Client\Model\GetPaymentIdentifierReferenceNumbersResponse
```

Pobranie listy faktur dla identyfikatora płatności

Pobranie listy faktur dla identyfikatora płatności

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePatnociApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$payment_identifier = 'payment_identifier_example'; // string

try {
    $result = $apiInstance->onlinePaymentPaymentIdentifierGetReferenceNumbers($payment_identifier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePatnociApi->onlinePaymentPaymentIdentifierGetReferenceNumbers: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **payment_identifier** | **string**|  | |

### Return type

[**\OpenAPI\Client\Model\GetPaymentIdentifierReferenceNumbersResponse**](../Model/GetPaymentIdentifierReferenceNumbersResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlinePaymentPaymentIdentifierRequest()`

```php
onlinePaymentPaymentIdentifierRequest($request_payment_identifier_request): \OpenAPI\Client\Model\RequestPaymentIdentifierResponse
```

Wygenerowanie identyfikatora płatności

Wygenerowanie identyfikatora płatności

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePatnociApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_payment_identifier_request = new \OpenAPI\Client\Model\RequestPaymentIdentifierRequest(); // \OpenAPI\Client\Model\RequestPaymentIdentifierRequest

try {
    $result = $apiInstance->onlinePaymentPaymentIdentifierRequest($request_payment_identifier_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePatnociApi->onlinePaymentPaymentIdentifierRequest: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **request_payment_identifier_request** | [**\OpenAPI\Client\Model\RequestPaymentIdentifierRequest**](../Model/RequestPaymentIdentifierRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\RequestPaymentIdentifierResponse**](../Model/RequestPaymentIdentifierResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
