# OpenAPI\Client\InterfejsyInteraktywnePowiadczeniaApi

All URIs are relative to https://ksef.mf.gov.pl/api, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**onlineCredentialsCredentialsContextGrant()**](InterfejsyInteraktywnePowiadczeniaApi.md#onlineCredentialsCredentialsContextGrant) | **POST** /online/Credentials/ContextGrant | Nadanie poświadczeń kontekstowych |
| [**onlineCredentialsCredentialsContextRevoke()**](InterfejsyInteraktywnePowiadczeniaApi.md#onlineCredentialsCredentialsContextRevoke) | **POST** /online/Credentials/ContextRevoke | Odebranie poświadczeń kontekstowych |
| [**onlineCredentialsCredentialsGrant()**](InterfejsyInteraktywnePowiadczeniaApi.md#onlineCredentialsCredentialsGrant) | **POST** /online/Credentials/Grant | Nadanie poświadczeń |
| [**onlineCredentialsCredentialsRevoke()**](InterfejsyInteraktywnePowiadczeniaApi.md#onlineCredentialsCredentialsRevoke) | **POST** /online/Credentials/Revoke | Odebranie poświadczeń |
| [**onlineCredentialsCredentialsStatus()**](InterfejsyInteraktywnePowiadczeniaApi.md#onlineCredentialsCredentialsStatus) | **GET** /online/Credentials/Status/{CredentialsElementReferenceNumber} | Sprawdzenie statusu poświadczeń |
| [**onlineCredentialsGenerateToken()**](InterfejsyInteraktywnePowiadczeniaApi.md#onlineCredentialsGenerateToken) | **POST** /online/Credentials/GenerateToken | Generowanie tokena autoryzacyjnego |
| [**onlineCredentialsRevokeToken()**](InterfejsyInteraktywnePowiadczeniaApi.md#onlineCredentialsRevokeToken) | **POST** /online/Credentials/RevokeToken | Usunięcie tokena autoryzacyjnego |


## `onlineCredentialsCredentialsContextGrant()`

```php
onlineCredentialsCredentialsContextGrant($grant_context_credentials_request): \OpenAPI\Client\Model\StatusCredentialsResponse
```

Nadanie poświadczeń kontekstowych

Nadanie poświadczeń kontekstowych

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePowiadczeniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$grant_context_credentials_request = new \OpenAPI\Client\Model\GrantContextCredentialsRequest(); // \OpenAPI\Client\Model\GrantContextCredentialsRequest

try {
    $result = $apiInstance->onlineCredentialsCredentialsContextGrant($grant_context_credentials_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePowiadczeniaApi->onlineCredentialsCredentialsContextGrant: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **grant_context_credentials_request** | [**\OpenAPI\Client\Model\GrantContextCredentialsRequest**](../Model/GrantContextCredentialsRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\StatusCredentialsResponse**](../Model/StatusCredentialsResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineCredentialsCredentialsContextRevoke()`

```php
onlineCredentialsCredentialsContextRevoke($revoke_context_credentials_request): \OpenAPI\Client\Model\StatusCredentialsResponse
```

Odebranie poświadczeń kontekstowych

Odebranie poświadczeń kontekstowych

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePowiadczeniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$revoke_context_credentials_request = new \OpenAPI\Client\Model\RevokeContextCredentialsRequest(); // \OpenAPI\Client\Model\RevokeContextCredentialsRequest

try {
    $result = $apiInstance->onlineCredentialsCredentialsContextRevoke($revoke_context_credentials_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePowiadczeniaApi->onlineCredentialsCredentialsContextRevoke: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **revoke_context_credentials_request** | [**\OpenAPI\Client\Model\RevokeContextCredentialsRequest**](../Model/RevokeContextCredentialsRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\StatusCredentialsResponse**](../Model/StatusCredentialsResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineCredentialsCredentialsGrant()`

```php
onlineCredentialsCredentialsGrant($grant_credentials_request): \OpenAPI\Client\Model\StatusCredentialsResponse
```

Nadanie poświadczeń

Nadanie poświadczeń

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePowiadczeniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$grant_credentials_request = new \OpenAPI\Client\Model\GrantCredentialsRequest(); // \OpenAPI\Client\Model\GrantCredentialsRequest

try {
    $result = $apiInstance->onlineCredentialsCredentialsGrant($grant_credentials_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePowiadczeniaApi->onlineCredentialsCredentialsGrant: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **grant_credentials_request** | [**\OpenAPI\Client\Model\GrantCredentialsRequest**](../Model/GrantCredentialsRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\StatusCredentialsResponse**](../Model/StatusCredentialsResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineCredentialsCredentialsRevoke()`

```php
onlineCredentialsCredentialsRevoke($revoke_credentials_request): \OpenAPI\Client\Model\StatusCredentialsResponse
```

Odebranie poświadczeń

Odebranie poświadczeń

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePowiadczeniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$revoke_credentials_request = new \OpenAPI\Client\Model\RevokeCredentialsRequest(); // \OpenAPI\Client\Model\RevokeCredentialsRequest

try {
    $result = $apiInstance->onlineCredentialsCredentialsRevoke($revoke_credentials_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePowiadczeniaApi->onlineCredentialsCredentialsRevoke: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **revoke_credentials_request** | [**\OpenAPI\Client\Model\RevokeCredentialsRequest**](../Model/RevokeCredentialsRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\StatusCredentialsResponse**](../Model/StatusCredentialsResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineCredentialsCredentialsStatus()`

```php
onlineCredentialsCredentialsStatus($credentials_element_reference_number): \OpenAPI\Client\Model\StatusCredentialsResponse
```

Sprawdzenie statusu poświadczeń

Sprawdzenie statusu poświadczeń

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePowiadczeniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$credentials_element_reference_number = 'credentials_element_reference_number_example'; // string

try {
    $result = $apiInstance->onlineCredentialsCredentialsStatus($credentials_element_reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePowiadczeniaApi->onlineCredentialsCredentialsStatus: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **credentials_element_reference_number** | **string**|  | |

### Return type

[**\OpenAPI\Client\Model\StatusCredentialsResponse**](../Model/StatusCredentialsResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineCredentialsGenerateToken()`

```php
onlineCredentialsGenerateToken($generate_token_request): \OpenAPI\Client\Model\GenerateTokenResponse
```

Generowanie tokena autoryzacyjnego

Generowanie tokena autoryzacyjnego

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePowiadczeniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$generate_token_request = new \OpenAPI\Client\Model\GenerateTokenRequest(); // \OpenAPI\Client\Model\GenerateTokenRequest

try {
    $result = $apiInstance->onlineCredentialsGenerateToken($generate_token_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePowiadczeniaApi->onlineCredentialsGenerateToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **generate_token_request** | [**\OpenAPI\Client\Model\GenerateTokenRequest**](../Model/GenerateTokenRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\GenerateTokenResponse**](../Model/GenerateTokenResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineCredentialsRevokeToken()`

```php
onlineCredentialsRevokeToken($revoke_token_request): \OpenAPI\Client\Model\StatusCredentialsResponse
```

Usunięcie tokena autoryzacyjnego

Odebranie Usunięcie tokena autoryzacyjnego

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywnePowiadczeniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$revoke_token_request = new \OpenAPI\Client\Model\RevokeTokenRequest(); // \OpenAPI\Client\Model\RevokeTokenRequest

try {
    $result = $apiInstance->onlineCredentialsRevokeToken($revoke_token_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywnePowiadczeniaApi->onlineCredentialsRevokeToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **revoke_token_request** | [**\OpenAPI\Client\Model\RevokeTokenRequest**](../Model/RevokeTokenRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\StatusCredentialsResponse**](../Model/StatusCredentialsResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`, `application/vnd.v2+json`
- **Accept**: `application/json`, `application/vnd.v2+json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
