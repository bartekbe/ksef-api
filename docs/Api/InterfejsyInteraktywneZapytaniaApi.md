# OpenAPI\Client\InterfejsyInteraktywneZapytaniaApi

All URIs are relative to https://ksef.mf.gov.pl/api, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**onlineQueryQueryCredentials()**](InterfejsyInteraktywneZapytaniaApi.md#onlineQueryQueryCredentials) | **POST** /online/Query/Credential/Sync | Zapytanie o poświadczenia |
| [**onlineQueryQueryInvoice()**](InterfejsyInteraktywneZapytaniaApi.md#onlineQueryQueryInvoice) | **POST** /online/Query/Invoice/Sync | Zapytanie o faktury |
| [**onlineQueryQueryInvoiceFetch()**](InterfejsyInteraktywneZapytaniaApi.md#onlineQueryQueryInvoiceFetch) | **GET** /online/Query/Invoice/Async/Fetch/{QueryElementReferenceNumber}/{PartElementReferenceNumber} | Pobranie wyników zapytania o faktury |
| [**onlineQueryQueryInvoiceInit()**](InterfejsyInteraktywneZapytaniaApi.md#onlineQueryQueryInvoiceInit) | **POST** /online/Query/Invoice/Async/Init | Inicjalizacja zapytania o faktury |
| [**onlineQueryQueryInvoiceStatus()**](InterfejsyInteraktywneZapytaniaApi.md#onlineQueryQueryInvoiceStatus) | **GET** /online/Query/Invoice/Async/Status/{QueryElementReferenceNumber} | Sprawdzenie statusu zapytania o faktury |


## `onlineQueryQueryCredentials()`

```php
onlineQueryQueryCredentials($query_sync_credentials_request): \OpenAPI\Client\Model\QuerySyncCredentialsResponse
```

Zapytanie o poświadczenia

Zapytanie o poświadczenia

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneZapytaniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$query_sync_credentials_request = new \OpenAPI\Client\Model\QuerySyncCredentialsRequest(); // \OpenAPI\Client\Model\QuerySyncCredentialsRequest

try {
    $result = $apiInstance->onlineQueryQueryCredentials($query_sync_credentials_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneZapytaniaApi->onlineQueryQueryCredentials: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query_sync_credentials_request** | [**\OpenAPI\Client\Model\QuerySyncCredentialsRequest**](../Model/QuerySyncCredentialsRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\QuerySyncCredentialsResponse**](../Model/QuerySyncCredentialsResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineQueryQueryInvoice()`

```php
onlineQueryQueryInvoice($page_size, $page_offset, $query_invoice_request): \OpenAPI\Client\Model\QueryInvoiceSyncResponse
```

Zapytanie o faktury

Zapytanie o faktury

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneZapytaniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_size = 56; // int
$page_offset = 56; // int
$query_invoice_request = new \OpenAPI\Client\Model\QueryInvoiceRequest(); // \OpenAPI\Client\Model\QueryInvoiceRequest

try {
    $result = $apiInstance->onlineQueryQueryInvoice($page_size, $page_offset, $query_invoice_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneZapytaniaApi->onlineQueryQueryInvoice: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page_size** | **int**|  | |
| **page_offset** | **int**|  | |
| **query_invoice_request** | [**\OpenAPI\Client\Model\QueryInvoiceRequest**](../Model/QueryInvoiceRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\QueryInvoiceSyncResponse**](../Model/QueryInvoiceSyncResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`, `application/vnd.v2+json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineQueryQueryInvoiceFetch()`

```php
onlineQueryQueryInvoiceFetch($query_element_reference_number, $part_element_reference_number): object
```

Pobranie wyników zapytania o faktury

Pobranie wyników zapytania o faktury

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneZapytaniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$query_element_reference_number = 'query_element_reference_number_example'; // string
$part_element_reference_number = 'part_element_reference_number_example'; // string

try {
    $result = $apiInstance->onlineQueryQueryInvoiceFetch($query_element_reference_number, $part_element_reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneZapytaniaApi->onlineQueryQueryInvoiceFetch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query_element_reference_number** | **string**|  | |
| **part_element_reference_number** | **string**|  | |

### Return type

**object**

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/octet-stream`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineQueryQueryInvoiceInit()`

```php
onlineQueryQueryInvoiceInit($query_invoice_request): \OpenAPI\Client\Model\QueryInvoiceAsyncInitResponse
```

Inicjalizacja zapytania o faktury

Inicjalizacja zapytania o faktury

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneZapytaniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$query_invoice_request = new \OpenAPI\Client\Model\QueryInvoiceRequest(); // \OpenAPI\Client\Model\QueryInvoiceRequest

try {
    $result = $apiInstance->onlineQueryQueryInvoiceInit($query_invoice_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneZapytaniaApi->onlineQueryQueryInvoiceInit: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query_invoice_request** | [**\OpenAPI\Client\Model\QueryInvoiceRequest**](../Model/QueryInvoiceRequest.md)|  | |

### Return type

[**\OpenAPI\Client\Model\QueryInvoiceAsyncInitResponse**](../Model/QueryInvoiceAsyncInitResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: `application/json`, `application/vnd.v2+json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `onlineQueryQueryInvoiceStatus()`

```php
onlineQueryQueryInvoiceStatus($query_element_reference_number): \OpenAPI\Client\Model\QueryInvoiceAsyncStatusResponse
```

Sprawdzenie statusu zapytania o faktury

Sprawdzenie statusu zapytania o faktury

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: SessionToken
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('SessionToken', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('SessionToken', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\InterfejsyInteraktywneZapytaniaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$query_element_reference_number = 'query_element_reference_number_example'; // string

try {
    $result = $apiInstance->onlineQueryQueryInvoiceStatus($query_element_reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InterfejsyInteraktywneZapytaniaApi->onlineQueryQueryInvoiceStatus: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query_element_reference_number** | **string**|  | |

### Return type

[**\OpenAPI\Client\Model\QueryInvoiceAsyncStatusResponse**](../Model/QueryInvoiceAsyncStatusResponse.md)

### Authorization

[SessionToken](../../README.md#SessionToken)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
