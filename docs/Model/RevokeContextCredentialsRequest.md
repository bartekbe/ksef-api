# # RevokeContextCredentialsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revoke_context_credentials** | [**\OpenAPI\Client\Model\RevokeContextCredentialsRequestType**](RevokeContextCredentialsRequestType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
