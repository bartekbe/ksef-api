# # CredentialsRoleResponseBaseTypeObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role_description** | **string** |  | [optional]
**role_type** | **object** |  | [optional]
**start_timestamp** | **\DateTime** |  |
**type** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
