# # SessionInvoiceStatusType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acquisition_timestamp** | **\DateTime** |  | [optional]
**element_reference_number** | **string** |  |
**invoice_number** | **string** |  | [optional]
**ksef_reference_number** | **string** |  | [optional]
**processing_code** | **int** |  |
**processing_description** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
