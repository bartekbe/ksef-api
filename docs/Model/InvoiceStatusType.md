# # InvoiceStatusType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acquisition_timestamp** | **\DateTime** |  | [optional]
**invoice_number** | **string** |  | [optional]
**ksef_reference_number** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
