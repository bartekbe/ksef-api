# # RevokeContextCredentialsRequestType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierRequestInstitutionalType**](CredentialsIdentifierRequestInstitutionalType.md) |  |
**credentials_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierRequestIndividualType**](CredentialsIdentifierRequestIndividualType.md) |  |
**credentials_role** | [**\OpenAPI\Client\Model\CredentialsRoleRequestContextBaseType**](CredentialsRoleRequestContextBaseType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
