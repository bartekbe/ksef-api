# # QueryInvoiceSyncResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_header_list** | [**\OpenAPI\Client\Model\InvoiceHeaderType[]**](InvoiceHeaderType.md) |  |
**number_of_elements** | **int** |  |
**page_offset** | **int** |  |
**page_size** | **int** |  |
**reference_number** | **string** |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
