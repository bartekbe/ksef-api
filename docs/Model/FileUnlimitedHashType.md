# # FileUnlimitedHashType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_size** | **int** |  |
**hash_sha** | [**\OpenAPI\Client\Model\HashSHAType**](HashSHAType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
