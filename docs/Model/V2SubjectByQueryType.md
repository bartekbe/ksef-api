# # V2SubjectByQueryType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issued_by_identifier** | [**\OpenAPI\Client\Model\SubjectIdentifierByType**](SubjectIdentifierByType.md) |  |
**issued_by_name** | [**\OpenAPI\Client\Model\SubjectNameType**](SubjectNameType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
