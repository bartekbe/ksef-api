# # RevokeCredentialsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revoke_credentials** | [**\OpenAPI\Client\Model\RevokeCredentialsRequestType**](RevokeCredentialsRequestType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
