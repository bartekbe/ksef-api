# # V2RevokeTokenRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revoke_token** | [**\OpenAPI\Client\Model\V2RevokeTokenRequestType**](V2RevokeTokenRequestType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
