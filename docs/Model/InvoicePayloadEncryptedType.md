# # InvoicePayloadEncryptedType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encrypted_invoice_body** | **\SplFileObject** |  |
**encrypted_invoice_hash** | [**\OpenAPI\Client\Model\File2MBHashType**](File2MBHashType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
