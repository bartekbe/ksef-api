# # V2CredentialsRoleResponseGrantedByInstitutionType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role_grantor_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierResponseInstitutionalNipType**](CredentialsIdentifierResponseInstitutionalNipType.md) |  | [optional]
**role_type** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
