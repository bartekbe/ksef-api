# # GrantCredentialsRequestType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierRequestType**](CredentialsIdentifierRequestType.md) |  |
**credentials_role_list** | [**\OpenAPI\Client\Model\CredentialsRoleRequestStandardDescribedType[]**](CredentialsRoleRequestStandardDescribedType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
