# # ExceptionType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exception_detail_list** | [**\OpenAPI\Client\Model\ExceptionDetailType[]**](ExceptionDetailType.md) |  |
**reference_number** | **string** |  | [optional]
**service_code** | **string** |  |
**service_ctx** | **string** |  |
**service_name** | **string** |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
