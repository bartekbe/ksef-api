# # GenerateTokenRequestType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials_role_list** | [**\OpenAPI\Client\Model\CredentialsRoleRequestTokenType[]**](CredentialsRoleRequestTokenType.md) |  |
**description** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
