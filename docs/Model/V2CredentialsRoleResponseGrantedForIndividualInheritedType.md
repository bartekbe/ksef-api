# # V2CredentialsRoleResponseGrantedForIndividualInheritedType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**\OpenAPI\Client\Model\V2CredentialsRoleGrantedForInstitutionInheritanceType**](V2CredentialsRoleGrantedForInstitutionInheritanceType.md) |  | [optional]
**role_type** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
