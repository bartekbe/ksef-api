# # InvoiceHeaderType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acquisition_timestamp** | **\DateTime** |  |
**currency** | **string** |  |
**gross** | **string** |  |
**invoice_hash** | [**\OpenAPI\Client\Model\FileUnlimitedHashType**](FileUnlimitedHashType.md) |  |
**invoice_reference_number** | **string** |  |
**invoicing_date** | **\DateTime** |  |
**ksef_reference_number** | **string** |  |
**net** | **string** |  |
**subject_by** | [**\OpenAPI\Client\Model\SubjectByType**](SubjectByType.md) |  |
**subject_by_k** | [**\OpenAPI\Client\Model\SubjectByType**](SubjectByType.md) |  | [optional]
**subject_to** | [**\OpenAPI\Client\Model\SubjectToType**](SubjectToType.md) |  |
**subject_to_k_list** | [**\OpenAPI\Client\Model\SubjectToType[]**](SubjectToType.md) |  | [optional]
**subjects_authorized_list** | [**\OpenAPI\Client\Model\SubjectAuthorizedType[]**](SubjectAuthorizedType.md) |  | [optional]
**subjects_other_list** | [**\OpenAPI\Client\Model\SubjectOtherType[]**](SubjectOtherType.md) |  | [optional]
**vat** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
