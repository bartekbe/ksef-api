# # GrantContextCredentialsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grant_context_credentials** | [**\OpenAPI\Client\Model\GrantContextCredentialsRequestType**](GrantContextCredentialsRequestType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
