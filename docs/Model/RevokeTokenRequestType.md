# # RevokeTokenRequestType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_token_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierRequestType**](CredentialsIdentifierRequestType.md) |  |
**token_number** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
