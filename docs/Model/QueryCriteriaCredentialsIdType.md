# # QueryCriteriaCredentialsIdType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierRequestType**](CredentialsIdentifierRequestType.md) |  |
**query_credentials_scope_result_type** | **string** |  |
**query_credentials_type_result_type** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
