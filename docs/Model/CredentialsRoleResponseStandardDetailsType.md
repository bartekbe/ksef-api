# # CredentialsRoleResponseStandardDetailsType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials_assignment_type** | **string** |  | [optional]
**role_assigning_author_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierResponseType**](CredentialsIdentifierResponseType.md) |  | [optional]
**role_type** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
