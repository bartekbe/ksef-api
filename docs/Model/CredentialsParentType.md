# # CredentialsParentType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials_role** | [**\OpenAPI\Client\Model\CredentialsRoleResponseStandardPlainType**](CredentialsRoleResponseStandardPlainType.md) |  | [optional]
**identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierResponseType**](CredentialsIdentifierResponseType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
