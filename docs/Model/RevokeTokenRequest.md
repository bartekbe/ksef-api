# # RevokeTokenRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revoke_token** | [**\OpenAPI\Client\Model\RevokeTokenRequestType**](RevokeTokenRequestType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
