# # SendInvoiceRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_hash** | [**\OpenAPI\Client\Model\File1MBHashType**](File1MBHashType.md) |  |
**invoice_payload** | [**\OpenAPI\Client\Model\InvoicePayloadType**](InvoicePayloadType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
