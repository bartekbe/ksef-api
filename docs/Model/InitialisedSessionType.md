# # InitialisedSessionType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | [**\OpenAPI\Client\Model\SessionContextType**](SessionContextType.md) |  |
**token** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
