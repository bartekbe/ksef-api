# # QueryInvoiceAsyncInitResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**element_reference_number** | **string** |  |
**processing_code** | **int** |  |
**processing_description** | **string** |  |
**reference_number** | **string** |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
