# # InitSessionResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_number** | **string** |  |
**session_token** | [**\OpenAPI\Client\Model\InitialisedSessionType**](InitialisedSessionType.md) |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
