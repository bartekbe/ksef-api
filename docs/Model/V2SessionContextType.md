# # V2SessionContextType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context_identifier** | [**\OpenAPI\Client\Model\SubjectIdentifierByType**](SubjectIdentifierByType.md) |  |
**context_name** | [**\OpenAPI\Client\Model\SubjectNameType**](SubjectNameType.md) |  | [optional]
**credentials_role_list** | [**\OpenAPI\Client\Model\V2CredentialsRoleResponseBaseTypeObject[]**](V2CredentialsRoleResponseBaseTypeObject.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
