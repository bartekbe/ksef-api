# # V2CredentialsRoleGrantedForInstitutionInheritanceType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grantee_identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierResponseInstitutionalNipType**](CredentialsIdentifierResponseInstitutionalNipType.md) |  | [optional]
**role_description** | **string** |  | [optional]
**role_type** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
