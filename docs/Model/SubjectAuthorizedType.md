# # SubjectAuthorizedType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issued_to_identifier** | [**\OpenAPI\Client\Model\SubjectIdentifierToType**](SubjectIdentifierToType.md) |  |
**issued_to_name** | [**\OpenAPI\Client\Model\SubjectNameType**](SubjectNameType.md) |  | [optional]
**subject_authorized_type** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
