# # QuerySyncCredentialsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials_list** | [**\OpenAPI\Client\Model\CredentialsBaseTypeObject[]**](CredentialsBaseTypeObject.md) |  |
**reference_number** | **string** |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
