# # V2QueryCriteriaInvoiceRangeType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoicing_date_from** | **\DateTime** | yyyy-MM-dd&#39;T&#39;HH:mm:ss | minimum date range is 2022-01-01 |
**invoicing_date_to** | **\DateTime** | yyyy-MM-dd&#39;T&#39;HH:mm:ss | maximum date range is current time (+ max 6 hours), the difference between date field and #invoicingDateFrom cannot be greater than 24 months, date field cannot be before #invoicingDateFrom |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
