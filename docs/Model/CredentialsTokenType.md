# # CredentialsTokenType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials_role_list** | [**\OpenAPI\Client\Model\CredentialsRoleResponseTokenType[]**](CredentialsRoleResponseTokenType.md) |  | [optional]
**description** | **string** |  | [optional]
**identifier** | [**\OpenAPI\Client\Model\CredentialsIdentifierResponseAuthorisationTokenType**](CredentialsIdentifierResponseAuthorisationTokenType.md) |  | [optional]
**last_use_timestamp** | **\DateTime** |  |
**parent** | [**\OpenAPI\Client\Model\CredentialsPlainType**](CredentialsPlainType.md) |  | [optional]
**registration_timestamp** | **\DateTime** |  |
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
