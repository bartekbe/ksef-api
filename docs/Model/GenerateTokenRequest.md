# # GenerateTokenRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**generate_token** | [**\OpenAPI\Client\Model\GenerateTokenRequestType**](GenerateTokenRequestType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
