# # CredentialsBaseTypeObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **object** |  | [optional]
**type** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
