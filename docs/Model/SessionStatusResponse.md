# # SessionStatusResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_status_list** | [**\OpenAPI\Client\Model\SessionInvoiceStatusType[]**](SessionInvoiceStatusType.md) |  | [optional]
**number_of_elements** | **int** |  | [optional]
**page_offset** | **int** |  | [optional]
**page_size** | **int** |  | [optional]
**processing_code** | **int** |  |
**processing_description** | **string** |  |
**reference_number** | **string** |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
