# # RequestPaymentIdentifierResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_identifier** | **string** |  |
**reference_number** | **string** |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
