# # InvoiceDivisionPlainPartType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**part_expiration** | **\DateTime** |  |
**part_name** | **string** |  |
**part_number** | **int** |  |
**part_range_from** | **\DateTime** |  |
**part_range_to** | **\DateTime** |  |
**part_reference_number** | **string** |  |
**plain_part_hash** | [**\OpenAPI\Client\Model\FileUnlimitedHashType**](FileUnlimitedHashType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
