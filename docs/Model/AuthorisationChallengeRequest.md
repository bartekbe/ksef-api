# # AuthorisationChallengeRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context_identifier** | [**\OpenAPI\Client\Model\SubjectIdentifierByType**](SubjectIdentifierByType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
