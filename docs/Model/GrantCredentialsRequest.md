# # GrantCredentialsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grant_credentials** | [**\OpenAPI\Client\Model\GrantCredentialsRequestType**](GrantCredentialsRequestType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
