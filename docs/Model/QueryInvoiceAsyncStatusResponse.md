# # QueryInvoiceAsyncStatusResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**division_type** | **string** |  | [optional]
**element_reference_number** | **string** |  |
**number_of_elements** | **int** |  | [optional]
**number_of_parts** | **int** |  | [optional]
**part_list** | [**\OpenAPI\Client\Model\InvoiceDivisionPlainPartType[]**](InvoiceDivisionPlainPartType.md) |  | [optional]
**processing_code** | **int** |  |
**processing_description** | **string** |  |
**reference_number** | **string** |  |
**timestamp** | **\DateTime** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
