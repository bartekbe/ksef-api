# # ExceptionDetailType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exception_code** | **int** |  |
**exception_description** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
