# # QueryInvoiceRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query_criteria** | [**\OpenAPI\Client\Model\QueryCriteriaInvoiceType**](QueryCriteriaInvoiceType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
