<?php
/**
 * QueryInvoiceSyncResponse
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * KSeF
 *
 * Krajowy System e-Faktur
 *
 * The version of the OpenAPI document: 1.1.3
 * Contact: info.ksef@mf.gov.pl
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.0.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * QueryInvoiceSyncResponse Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class QueryInvoiceSyncResponse implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'QueryInvoiceSyncResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'invoice_header_list' => '\OpenAPI\Client\Model\InvoiceHeaderType[]',
        'number_of_elements' => 'int',
        'page_offset' => 'int',
        'page_size' => 'int',
        'reference_number' => 'string',
        'timestamp' => '\DateTime'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'invoice_header_list' => null,
        'number_of_elements' => 'int64',
        'page_offset' => 'int32',
        'page_size' => 'int32',
        'reference_number' => null,
        'timestamp' => 'date-time'
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'invoice_header_list' => false,
		'number_of_elements' => false,
		'page_offset' => false,
		'page_size' => false,
		'reference_number' => false,
		'timestamp' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'invoice_header_list' => 'invoiceHeaderList',
        'number_of_elements' => 'numberOfElements',
        'page_offset' => 'pageOffset',
        'page_size' => 'pageSize',
        'reference_number' => 'referenceNumber',
        'timestamp' => 'timestamp'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'invoice_header_list' => 'setInvoiceHeaderList',
        'number_of_elements' => 'setNumberOfElements',
        'page_offset' => 'setPageOffset',
        'page_size' => 'setPageSize',
        'reference_number' => 'setReferenceNumber',
        'timestamp' => 'setTimestamp'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'invoice_header_list' => 'getInvoiceHeaderList',
        'number_of_elements' => 'getNumberOfElements',
        'page_offset' => 'getPageOffset',
        'page_size' => 'getPageSize',
        'reference_number' => 'getReferenceNumber',
        'timestamp' => 'getTimestamp'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('invoice_header_list', $data ?? [], null);
        $this->setIfExists('number_of_elements', $data ?? [], null);
        $this->setIfExists('page_offset', $data ?? [], null);
        $this->setIfExists('page_size', $data ?? [], null);
        $this->setIfExists('reference_number', $data ?? [], null);
        $this->setIfExists('timestamp', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['invoice_header_list'] === null) {
            $invalidProperties[] = "'invoice_header_list' can't be null";
        }
        if ((count($this->container['invoice_header_list']) > 100)) {
            $invalidProperties[] = "invalid value for 'invoice_header_list', number of items must be less than or equal to 100.";
        }

        if ((count($this->container['invoice_header_list']) < 0)) {
            $invalidProperties[] = "invalid value for 'invoice_header_list', number of items must be greater than or equal to 0.";
        }

        if ($this->container['number_of_elements'] === null) {
            $invalidProperties[] = "'number_of_elements' can't be null";
        }
        if (($this->container['number_of_elements'] < 0)) {
            $invalidProperties[] = "invalid value for 'number_of_elements', must be bigger than or equal to 0.";
        }

        if ($this->container['page_offset'] === null) {
            $invalidProperties[] = "'page_offset' can't be null";
        }
        if (($this->container['page_offset'] > 100)) {
            $invalidProperties[] = "invalid value for 'page_offset', must be smaller than or equal to 100.";
        }

        if (($this->container['page_offset'] < 0)) {
            $invalidProperties[] = "invalid value for 'page_offset', must be bigger than or equal to 0.";
        }

        if ($this->container['page_size'] === null) {
            $invalidProperties[] = "'page_size' can't be null";
        }
        if (($this->container['page_size'] > 100)) {
            $invalidProperties[] = "invalid value for 'page_size', must be smaller than or equal to 100.";
        }

        if (($this->container['page_size'] < 10)) {
            $invalidProperties[] = "invalid value for 'page_size', must be bigger than or equal to 10.";
        }

        if ($this->container['reference_number'] === null) {
            $invalidProperties[] = "'reference_number' can't be null";
        }
        if (!preg_match("/(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/", $this->container['reference_number'])) {
            $invalidProperties[] = "invalid value for 'reference_number', must be conform to the pattern /(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/.";
        }

        if ($this->container['timestamp'] === null) {
            $invalidProperties[] = "'timestamp' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets invoice_header_list
     *
     * @return \OpenAPI\Client\Model\InvoiceHeaderType[]
     */
    public function getInvoiceHeaderList()
    {
        return $this->container['invoice_header_list'];
    }

    /**
     * Sets invoice_header_list
     *
     * @param \OpenAPI\Client\Model\InvoiceHeaderType[] $invoice_header_list invoice_header_list
     *
     * @return self
     */
    public function setInvoiceHeaderList($invoice_header_list)
    {
        if (is_null($invoice_header_list)) {
            throw new \InvalidArgumentException('non-nullable invoice_header_list cannot be null');
        }

        if ((count($invoice_header_list) > 100)) {
            throw new \InvalidArgumentException('invalid value for $invoice_header_list when calling QueryInvoiceSyncResponse., number of items must be less than or equal to 100.');
        }
        if ((count($invoice_header_list) < 0)) {
            throw new \InvalidArgumentException('invalid length for $invoice_header_list when calling QueryInvoiceSyncResponse., number of items must be greater than or equal to 0.');
        }
        $this->container['invoice_header_list'] = $invoice_header_list;

        return $this;
    }

    /**
     * Gets number_of_elements
     *
     * @return int
     */
    public function getNumberOfElements()
    {
        return $this->container['number_of_elements'];
    }

    /**
     * Sets number_of_elements
     *
     * @param int $number_of_elements number_of_elements
     *
     * @return self
     */
    public function setNumberOfElements($number_of_elements)
    {
        if (is_null($number_of_elements)) {
            throw new \InvalidArgumentException('non-nullable number_of_elements cannot be null');
        }

        if (($number_of_elements < 0)) {
            throw new \InvalidArgumentException('invalid value for $number_of_elements when calling QueryInvoiceSyncResponse., must be bigger than or equal to 0.');
        }

        $this->container['number_of_elements'] = $number_of_elements;

        return $this;
    }

    /**
     * Gets page_offset
     *
     * @return int
     */
    public function getPageOffset()
    {
        return $this->container['page_offset'];
    }

    /**
     * Sets page_offset
     *
     * @param int $page_offset page_offset
     *
     * @return self
     */
    public function setPageOffset($page_offset)
    {
        if (is_null($page_offset)) {
            throw new \InvalidArgumentException('non-nullable page_offset cannot be null');
        }

        if (($page_offset > 100)) {
            throw new \InvalidArgumentException('invalid value for $page_offset when calling QueryInvoiceSyncResponse., must be smaller than or equal to 100.');
        }
        if (($page_offset < 0)) {
            throw new \InvalidArgumentException('invalid value for $page_offset when calling QueryInvoiceSyncResponse., must be bigger than or equal to 0.');
        }

        $this->container['page_offset'] = $page_offset;

        return $this;
    }

    /**
     * Gets page_size
     *
     * @return int
     */
    public function getPageSize()
    {
        return $this->container['page_size'];
    }

    /**
     * Sets page_size
     *
     * @param int $page_size page_size
     *
     * @return self
     */
    public function setPageSize($page_size)
    {
        if (is_null($page_size)) {
            throw new \InvalidArgumentException('non-nullable page_size cannot be null');
        }

        if (($page_size > 100)) {
            throw new \InvalidArgumentException('invalid value for $page_size when calling QueryInvoiceSyncResponse., must be smaller than or equal to 100.');
        }
        if (($page_size < 10)) {
            throw new \InvalidArgumentException('invalid value for $page_size when calling QueryInvoiceSyncResponse., must be bigger than or equal to 10.');
        }

        $this->container['page_size'] = $page_size;

        return $this;
    }

    /**
     * Gets reference_number
     *
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->container['reference_number'];
    }

    /**
     * Sets reference_number
     *
     * @param string $reference_number reference_number
     *
     * @return self
     */
    public function setReferenceNumber($reference_number)
    {
        if (is_null($reference_number)) {
            throw new \InvalidArgumentException('non-nullable reference_number cannot be null');
        }

        if ((!preg_match("/(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/", $reference_number))) {
            throw new \InvalidArgumentException("invalid value for \$reference_number when calling QueryInvoiceSyncResponse., must conform to the pattern /(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/.");
        }

        $this->container['reference_number'] = $reference_number;

        return $this;
    }

    /**
     * Gets timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->container['timestamp'];
    }

    /**
     * Sets timestamp
     *
     * @param \DateTime $timestamp timestamp
     *
     * @return self
     */
    public function setTimestamp($timestamp)
    {
        if (is_null($timestamp)) {
            throw new \InvalidArgumentException('non-nullable timestamp cannot be null');
        }
        $this->container['timestamp'] = $timestamp;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


