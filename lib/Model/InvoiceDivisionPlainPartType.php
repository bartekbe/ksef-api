<?php
/**
 * InvoiceDivisionPlainPartType
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * KSeF
 *
 * Krajowy System e-Faktur
 *
 * The version of the OpenAPI document: 1.1.3
 * Contact: info.ksef@mf.gov.pl
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.0.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * InvoiceDivisionPlainPartType Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class InvoiceDivisionPlainPartType implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'InvoiceDivisionPlainPartType';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'part_expiration' => '\DateTime',
        'part_name' => 'string',
        'part_number' => 'int',
        'part_range_from' => '\DateTime',
        'part_range_to' => '\DateTime',
        'part_reference_number' => 'string',
        'plain_part_hash' => '\OpenAPI\Client\Model\FileUnlimitedHashType'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'part_expiration' => 'date-time',
        'part_name' => null,
        'part_number' => 'int32',
        'part_range_from' => 'date-time',
        'part_range_to' => 'date-time',
        'part_reference_number' => null,
        'plain_part_hash' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'part_expiration' => false,
		'part_name' => false,
		'part_number' => false,
		'part_range_from' => false,
		'part_range_to' => false,
		'part_reference_number' => false,
		'plain_part_hash' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'part_expiration' => 'partExpiration',
        'part_name' => 'partName',
        'part_number' => 'partNumber',
        'part_range_from' => 'partRangeFrom',
        'part_range_to' => 'partRangeTo',
        'part_reference_number' => 'partReferenceNumber',
        'plain_part_hash' => 'plainPartHash'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'part_expiration' => 'setPartExpiration',
        'part_name' => 'setPartName',
        'part_number' => 'setPartNumber',
        'part_range_from' => 'setPartRangeFrom',
        'part_range_to' => 'setPartRangeTo',
        'part_reference_number' => 'setPartReferenceNumber',
        'plain_part_hash' => 'setPlainPartHash'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'part_expiration' => 'getPartExpiration',
        'part_name' => 'getPartName',
        'part_number' => 'getPartNumber',
        'part_range_from' => 'getPartRangeFrom',
        'part_range_to' => 'getPartRangeTo',
        'part_reference_number' => 'getPartReferenceNumber',
        'plain_part_hash' => 'getPlainPartHash'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('part_expiration', $data ?? [], null);
        $this->setIfExists('part_name', $data ?? [], null);
        $this->setIfExists('part_number', $data ?? [], null);
        $this->setIfExists('part_range_from', $data ?? [], null);
        $this->setIfExists('part_range_to', $data ?? [], null);
        $this->setIfExists('part_reference_number', $data ?? [], null);
        $this->setIfExists('plain_part_hash', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['part_expiration'] === null) {
            $invalidProperties[] = "'part_expiration' can't be null";
        }
        if ($this->container['part_name'] === null) {
            $invalidProperties[] = "'part_name' can't be null";
        }
        if ((mb_strlen($this->container['part_name']) > 100)) {
            $invalidProperties[] = "invalid value for 'part_name', the character length must be smaller than or equal to 100.";
        }

        if ((mb_strlen($this->container['part_name']) < 1)) {
            $invalidProperties[] = "invalid value for 'part_name', the character length must be bigger than or equal to 1.";
        }

        if ($this->container['part_number'] === null) {
            $invalidProperties[] = "'part_number' can't be null";
        }
        if (($this->container['part_number'] < 0)) {
            $invalidProperties[] = "invalid value for 'part_number', must be bigger than or equal to 0.";
        }

        if ($this->container['part_range_from'] === null) {
            $invalidProperties[] = "'part_range_from' can't be null";
        }
        if ($this->container['part_range_to'] === null) {
            $invalidProperties[] = "'part_range_to' can't be null";
        }
        if ($this->container['part_reference_number'] === null) {
            $invalidProperties[] = "'part_reference_number' can't be null";
        }
        if (!preg_match("/(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/", $this->container['part_reference_number'])) {
            $invalidProperties[] = "invalid value for 'part_reference_number', must be conform to the pattern /(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/.";
        }

        if ($this->container['plain_part_hash'] === null) {
            $invalidProperties[] = "'plain_part_hash' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets part_expiration
     *
     * @return \DateTime
     */
    public function getPartExpiration()
    {
        return $this->container['part_expiration'];
    }

    /**
     * Sets part_expiration
     *
     * @param \DateTime $part_expiration part_expiration
     *
     * @return self
     */
    public function setPartExpiration($part_expiration)
    {
        if (is_null($part_expiration)) {
            throw new \InvalidArgumentException('non-nullable part_expiration cannot be null');
        }
        $this->container['part_expiration'] = $part_expiration;

        return $this;
    }

    /**
     * Gets part_name
     *
     * @return string
     */
    public function getPartName()
    {
        return $this->container['part_name'];
    }

    /**
     * Sets part_name
     *
     * @param string $part_name part_name
     *
     * @return self
     */
    public function setPartName($part_name)
    {
        if (is_null($part_name)) {
            throw new \InvalidArgumentException('non-nullable part_name cannot be null');
        }
        if ((mb_strlen($part_name) > 100)) {
            throw new \InvalidArgumentException('invalid length for $part_name when calling InvoiceDivisionPlainPartType., must be smaller than or equal to 100.');
        }
        if ((mb_strlen($part_name) < 1)) {
            throw new \InvalidArgumentException('invalid length for $part_name when calling InvoiceDivisionPlainPartType., must be bigger than or equal to 1.');
        }

        $this->container['part_name'] = $part_name;

        return $this;
    }

    /**
     * Gets part_number
     *
     * @return int
     */
    public function getPartNumber()
    {
        return $this->container['part_number'];
    }

    /**
     * Sets part_number
     *
     * @param int $part_number part_number
     *
     * @return self
     */
    public function setPartNumber($part_number)
    {
        if (is_null($part_number)) {
            throw new \InvalidArgumentException('non-nullable part_number cannot be null');
        }

        if (($part_number < 0)) {
            throw new \InvalidArgumentException('invalid value for $part_number when calling InvoiceDivisionPlainPartType., must be bigger than or equal to 0.');
        }

        $this->container['part_number'] = $part_number;

        return $this;
    }

    /**
     * Gets part_range_from
     *
     * @return \DateTime
     */
    public function getPartRangeFrom()
    {
        return $this->container['part_range_from'];
    }

    /**
     * Sets part_range_from
     *
     * @param \DateTime $part_range_from part_range_from
     *
     * @return self
     */
    public function setPartRangeFrom($part_range_from)
    {
        if (is_null($part_range_from)) {
            throw new \InvalidArgumentException('non-nullable part_range_from cannot be null');
        }
        $this->container['part_range_from'] = $part_range_from;

        return $this;
    }

    /**
     * Gets part_range_to
     *
     * @return \DateTime
     */
    public function getPartRangeTo()
    {
        return $this->container['part_range_to'];
    }

    /**
     * Sets part_range_to
     *
     * @param \DateTime $part_range_to part_range_to
     *
     * @return self
     */
    public function setPartRangeTo($part_range_to)
    {
        if (is_null($part_range_to)) {
            throw new \InvalidArgumentException('non-nullable part_range_to cannot be null');
        }
        $this->container['part_range_to'] = $part_range_to;

        return $this;
    }

    /**
     * Gets part_reference_number
     *
     * @return string
     */
    public function getPartReferenceNumber()
    {
        return $this->container['part_reference_number'];
    }

    /**
     * Sets part_reference_number
     *
     * @param string $part_reference_number part_reference_number
     *
     * @return self
     */
    public function setPartReferenceNumber($part_reference_number)
    {
        if (is_null($part_reference_number)) {
            throw new \InvalidArgumentException('non-nullable part_reference_number cannot be null');
        }

        if ((!preg_match("/(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/", $part_reference_number))) {
            throw new \InvalidArgumentException("invalid value for \$part_reference_number when calling InvoiceDivisionPlainPartType., must conform to the pattern /(20[2-9][0-9]|2[1-9][0-9]{2}|[3-9][0-9]{3})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([0-9A-Z]{2})-([0-9A-F]{10})-([0-9A-F]{10})-([0-9A-F]{2})/.");
        }

        $this->container['part_reference_number'] = $part_reference_number;

        return $this;
    }

    /**
     * Gets plain_part_hash
     *
     * @return \OpenAPI\Client\Model\FileUnlimitedHashType
     */
    public function getPlainPartHash()
    {
        return $this->container['plain_part_hash'];
    }

    /**
     * Sets plain_part_hash
     *
     * @param \OpenAPI\Client\Model\FileUnlimitedHashType $plain_part_hash plain_part_hash
     *
     * @return self
     */
    public function setPlainPartHash($plain_part_hash)
    {
        if (is_null($plain_part_hash)) {
            throw new \InvalidArgumentException('non-nullable plain_part_hash cannot be null');
        }
        $this->container['plain_part_hash'] = $plain_part_hash;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


